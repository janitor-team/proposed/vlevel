vlevel (0.5.1-4) UNRELEASED; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: (Closes: #923617)
    + Upstream expects $(CC) to contain a C++ compiler.
    + cross.patch: Make pkg-config substitutable.

  [ Axel Beckert ]
  * Add DEP-3 header to Helmut's patch.

 -- Axel Beckert <abe@debian.org>  Sat, 02 Mar 2019 22:33:45 +0100

vlevel (0.5.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Update Vcs-* headers for move to salsa.debian.org.
  * debian/watch: Use https protocol.

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org.

  [ Axel Beckert ]
  * Bump debhelper compatibility level to 12.
    + Use b-d on "debhelper-compat (= 12)" instead of debian/compat.
  * Declare compliance with Debian Policy 4.3.0. (No changes needed.)

 -- Axel Beckert <abe@debian.org>  Wed, 02 Jan 2019 07:30:04 +0100

vlevel (0.5.1-2) unstable; urgency=medium

  * Remove trailing whitespace from debian/rules.
  * Switch Vcs-* header and DEP5 format URL to HTTPS.
  * Enable all hardening build flags.
    + Extend fix-hardcoded-makefile-variables.patch to also fix
      vlevel-jack/Makefile.
  * Use …/pkg-info.mk and $(DEB_VERSION) instead of dpkg-parsechangelog.
  * Declare compliance with Debian Policy 4.1.1. (No changes needed.)
  * Bump debhelper compatibility 10.
    + Update versioned debhelper build-dependency accordingly.
    + Replace all calls to make in Makefiles with $(MAKE) to properly pass
      "-j", etc.
  * Set "Rules-Requires-Root: no".
  * Add symlink from vlevel-jack.1.gz to vlevel.1.gz.

 -- Axel Beckert <abe@debian.org>  Mon, 30 Oct 2017 01:04:13 +0100

vlevel (0.5.1-1) unstable; urgency=medium

  * New upstream release
    + Update patch
      - Rename vlevel-bin to vlevel in override_dh_auto_install instead of
        adding a new file to the patch.
      - Add usr/lib/ladspa to debian/dirs
    + Prevent dh_clean removing upstream's REAMDE.orig
    + Add new build-dependency on "libjack-jackd2-dev | libjack-dev"
    + Install README.md and README.orig instead of README as documentation
    + Update debian/copyright wrt. to paths, license, authors and years
      - The original upstream changed the license from GPL to LGPL in CVS
        after the 0.5 release and the fork is based upon the latest CVS
        checkout.
    + Suggest jackd as the package now also contains a JACK filter.
    + Update package description to mention new features added upstream.

 -- Axel Beckert <abe@debian.org>  Sun, 15 Mar 2015 22:10:38 +0100

vlevel (0.5-1) unstable; urgency=low

  * Initial release (Closes: #774230)
  * Add patch to fix hardcoded Makefile variables

 -- Axel Beckert <abe@debian.org>  Tue, 30 Dec 2014 18:31:18 +0100
